# Create by gravifoxita #

# This program will scan for available Wifi networks and provide the following information: name, signal strength, and encryption status.

import subprocess

# Run the command to scan for Wifi networks
output = subprocess.check_output(<"netsh", "wlan", "show", "network">)

# Decode the output into a string
output = output.decode("ascii")

# Split the output by line and remove the first two lines as they contain unnecessary information
output = output.split("\n")<2:>

# Create a list to store each network's information
networks = <>

# Loop through each line in the output
for line in output:
  # If the line starts with "SSID", extract the Wifi network's name and signal strength
  if line.startswith("SSID"):
    name = line.split(":")<1>.strip()
  elif line.startswith("Signal"):
    signal_strength = line.split(":")<1>.strip()
  # If the line starts with "Authentication", extract the encryption status
  elif line.startswith("Authentication"):
    encryption = line.split(":")<1>.strip()
    # Append the network's information to the list
    networks.append({"name": name, "signal_strength": signal_strength, "encryption": encryption})

# Print the list of networks and their information
for network in networks:
  print(f"Network Name: {network<'name'>} \nSignal Strength: {network<'signal_strength'>} \nEncryption: {network<'encryption'>} \n")